//Global game variables

var game = new engine('background: #000;', false); //None style and start manually

//Console logs
var logs = game.engineLogger;
//Logs enabled, but in production should be disabled: logs.disable();
logs.disable();

//Loading
game.loading = function(){
    
}

//When loaded
game.onload = function(){
    //Hide loading screen
    document.getElementById('loadingScreen').style = 'display: none;';

    //Load score from cookie
    bestScore = game.engineCookie.get('score');

    //Init stars
    initStars();
}

const vw = game.width / 100; //Constanta that contain 1% off width
const vh = game.height / 100; //Constanta that contain 1% off height

//Start position of player
const startX = 10*vw;
const startY = 40*vh;

//Game ticks
var ticks = 0;

//Font
var font = '30px Arial';

//Time slower
var time = 10;

//Difficulty
var difficulty = 1;

//Bonuses limit
const bonusesLimit = 3;

//Obstacle speed
const obstacleSpeed = 1;

//Score
const scoreX = 90*vw;
const scoreY = 10*vh;
var bestScore = 0;

//Constant forces
const playerJump = 0.5*vh;
const playerFall = 0.005*vh;

//Background speed and movement direction
var backgroundDirection = -1;
const backgroundSpeed = 0.05*vh * backgroundDirection;

//Bonuse forces
var bonuseJump = 0*vh;
var bonuseFall = 0*vh;
var invert = 1;
var slowTime = 1;
var slowTimeDuration = 50;

//Stars
var stars = [];
const Maxstars = 100;
const maxSpeed = 0.3*vw;
const minSpeed = 0.01*vw;

//Init stars
var initStars = function(){
    for(let i = 0; i < Maxstars; i++){
        stars.push(new game.circle({
            x: Math.random() * game.width,
            y: Math.random() * game.height,
            r: 0.3*vw,
            color: 'rgba(200,200,250,'+ (Math.random() * (1.0 - 0.3) + 0.3) +')',
            fill: true
        }));
        
        stars[stars.length - 1].speed_x = Math.random() * maxSpeed + minSpeed;
        stars[stars.length - 1].speed_y = 0; //Math.random() * maxSpeed + minSpeed * 0.1;

        stars[stars.length - 1].hover = function(){}
        stars[stars.length - 1].unhover = function(){}
        stars[stars.length - 1].onDown = function(){}
        stars[stars.length - 1].onUp = function(){}
    }
}

//Start the engine
game.start();