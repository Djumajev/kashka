//Bonuses

//Bonuse circle
var contor = [];

//Start x,y and radius
var startContorX = 6*vw;
var startContorY = 6*vw;
var contorRadius = 5*vw;

//Delete all bonuses and create empty circles
var resetBonuses = function(){
    contor = [];

    let x = startContorX;

    for(let i = 0; i < 2; i++) {
        contor.push(new game.circle({
            x: x,
            y: startContorY,
            r: contorRadius,
            color: 'green',
            fill: true,
            lineWidth: 2
        }));

        x += contorRadius * 3;
    }
}

//Draw bonuses top of screen
var drawBonuses = function(){
    for(let i = 0; i < contor.length; i++) {
        contor[i].draw();
    }
}

//Save bonuses
var saveBonuse = function(){
    contor[0].color = 'red';
    game.engineCookie.delete('bonuses');
    //game.engineCookie.save('bonuses', contor);
}

//First enter
/**
 * if something saved in cookie -> load
 * else create two slots
 */

let cookie = game.engineCookie.get('bonuses');
if(cookie) {
    contor = cookie;
} else {
    resetBonuses();
}

