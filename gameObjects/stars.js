//Stars 

//Draw stars on background
var drawStars = function(){
    //Move stars
    for(let i = 0; i < Maxstars; i++) {
        if(stars[i].x <= 0) {
            stars[i].x = game.width + Math.random() * 50*vw;
            stars[i].y = Math.random() * game.height;
        } else {
            let grad = 255 / difficulty;
            
            let red = 200;
            let green = 200;
            let blue = 200;

            if(difficulty < 40){
                red *= grad; 
                green *= grad;
                blue *= grad; 
            } else if(difficulty >= 40 && difficulty < 200) {
                blue *= grad;
            } else if(difficulty >= 200 && difficulty < 400) {
                red *= grad;
                blue *= grad;
            } else {
                red = 255;
                green = 180;
                blue = 150;
            }


            stars[i].color = 'rgba(' + red + ',' + green + ',' + blue + ','+ (Math.random() * (1.0 - 0.3) + 0.3) +')';
            stars[i].x -= stars[i].speed_x * time * difficulty * 0.03;
        }

        //Draw stars
        stars[i].draw();
    }

}


