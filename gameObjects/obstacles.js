//Obstacles

var obstacles = [];

/**
 * Add obstacle in x,y position, with width and height and image
 * 
 * @param x -> x position
 * @param y -> y position
 * @param width -> width
 * @param height -> height
 * @param image -> image from sprites{}
 */
var addObstacle = function(x, y, width, height, image){
    obstacles.push(new game.sprite({
        x: x,
        y: y,
        width: width,
        height: height,
        image: image,
        angle: 0
    }));

    //Rotation speed
    obstacles[obstacles.length - 1].rotationSpeed = Math.random()* (5 + 5) -5;
}

//Check player angles hits with obstacle
/**
 * @param object -> contain x,y,r
 * @return boolean 
 */
var isHited = function(object){
    //Left top
    if(game.pointInCircle({x: player.x, y: player.y}, object)) {
        return true;
    }

    //Right top
    if(game.pointInCircle({x: player.x + player.width, y: player.y}, object)) {
        return true;
    }

    //Left bottom
    if(game.pointInCircle({x: player.x, y: player.y + player.height}, object)) {
        return true;
    }

    //Right bottom
    if(game.pointInCircle({x: player.x + player.width, y: player.y + player.height}, object)) {
        return true;
    }

    //Else return false
    return false;
}


//Draw all obstacles and intersection with player
var drawObstacles = function(){
    obstacles.forEach(obstacle => {
        obstacle.x -= obstacleSpeed * time * difficulty * 0.1;

        obstacle.angle += obstacle.rotationSpeed * time;

        obstacle.draw();

        if(game.distanceBetweenTwoPoints(player, obstacle) < 30*vw) {
            tap.slowTime(slowTimeDuration, 0.5);        

            let col = {
                x: obstacle.x + obstacle.width/2,
                y: obstacle.y + obstacle.height/2,
                r: obstacle.width/2 - obstacle.width/20
            };

            //Intersection player and obstacle
            if(isHited(col)){
                player.dead();
            }
        }
    });
}