//Tap everywhere to jump

var tap = new game.strokeBox({
    x: 0,
    y: 0,
    button: true,
    width: 100*vw,
    height: 100*vh,
    color: '#FFFF'
});


//On click everywhere if player alive
tap.onDown = function(){
    player.speedY -= Math.cos(player.angle * Math.PI/180) * (playerJump + bonuseJump) * invert;
    this.slowTime(slowTimeDuration, slowTime);
}

//Slow time every time when tap for (ticks) ticks, for (times) times
tap.timeCounter = 0;
tap.slowed = false;
this.ticks = 0;
tap.slowTime = function(ticks, times){
    if(this.slowed) {
        return;
    }

    this.slowed = true;
    this.ticks = ticks;
    time = times;
}

//Control time slowing
tap.timeController = function(){
    if(!this.slowed) {
        return;
    }

    if(this.timeCounter >= this.ticks) {
        time = 1;
        this.timeCounter = 0;
        this.slowed = false;
        return;
    }

    this.timeCounter++;
}

//Tap + time control function
var tapDraw = function(){
    tap.draw();

    tap.timeController();
}