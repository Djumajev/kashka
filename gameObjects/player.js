//Player

var player = new game.sprite({
    x: startX,
    y: startY,
    image: sprites.player[0],
    width: 744/100 * vw,
    height: 840/90 * vw,
    angle: 0
});

//Is player alive
player.alive = false;

//Player force vector
player.speedY = 0;

//Collected bonuses
player.bonuses = [];

//Rotation speed
player.rotationSpeed = 3;

//Add bonuse
player.addBonuse = function(bonus){
    //Add bonuse
    bonuses.push(bonus);

    //Remove first one if bonuses == bonusesLimit
}

/**
 * If player alive do some stuf
 * 
 * @return true of player alive, else false 
 */
var drawPlayer = function(){
    if(!player.alive) {
        return false;
    }

    player.y += player.speedY * time;

    //Player of field
    if(player.y > game.height + 5*vh || player.y < 0 - 5*vh) {
        player.dead();
        return false;
    }

    player.angle += player.rotationSpeed * time;

    if(player.angle > 360) {
        player.angle = 0;
    }

    player.draw();

    //Falling
    player.speedY += (playerFall + bonuseFall) * invert;

    return true;
}

//On dead
player.dead = function(){
    //Kill player
    player.alive = false;

    player.angle = 0;
    player.y = startY;
    player.speedY = 0;

    //Clear all obstacles
    obstacles = [];

    difficulty = 1;
}