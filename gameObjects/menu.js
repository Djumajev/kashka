//User interface

var playButton = new game.sprite({
    x: 50*vw - 93/2.5*vw /2,
    y: 40*vh - 33/2.5*vw /2,
    width: 93/2.5*vw,
    height: 33/2.5*vw,
    image: sprites.playButton[0],
    button: true,
    angle: 0
});

var colider = new game.strokeBox({
    x: 50*vw - 93/2*vw /2,
    y: 40*vh - 33/2*vw /2,
    width: 93/2*vw,
    height: 33/2*vw,
    color: '#0F00',
    button: true
}); 

//Click play button
colider.onDown = function(){
    if(player.alive) {
        return;
    }

    player.alive = true;
    score.score = 0;
}

//Last score
var lastScore = new game.fillTextLine({
   x: 50*vw,
   y: 58*vh,
   color: '#FFF',
   align: 'center',
   font: 2*vh + 'px Arial'
});

//Best score
var bestScoreT = new game.fillTextLine({
    x: 50*vw,
    y: 55*vh,
    color: '#FFF',
    align: 'center',
    font: 2*vh + 'px Arial'
 });

var animDir = 1;

//Draw user interface
var drawUI = function(){
    loadBestScore();
    
    //Play button animation
    if(playButton.angle >= 5) {
        animDir = -1;
    } else if(playButton.angle <= -5) {
        animDir = 1;
    }

    playButton.angle += animDir / 8; //Dir / speed
    playButton.width += animDir / 8;
    playButton.height += animDir / 8;

    playButton.draw();
    colider.draw();

    /*
    if(score.score > 0) {
        lastScore.text = 'Last score: ' + Math.floor(score.score);
        lastScore.draw();
    }

    bestScoreT.text = 'Best score: ' + bestScore;
    bestScoreT.draw();*/
}