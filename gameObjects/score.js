//Score

var score = new game.fillTextLine({
    x: scoreX,
    y: scoreY,
    color: '#FFF',
    align: 'right',
    text: parseInt(bestScore),
    font: font
});
score.score = 0;

//Draw score
var drawScore = function(points){
    if(points != undefined) {
        score.score += points * time;
        score.text = Math.floor(score.score);
        difficulty += points; //temp
    }
    
    score.draw();
}

//Save score
var saveScore = function(){
    if(game.engineCookie.get('score') < score.score) {
        game.engineCookie.save('score', Math.floor(score.score));
    }
}

//Load best score
var loadBestScore = function(){
    bestScore = game.engineCookie.get('score');
}