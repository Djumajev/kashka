//Update function

game.update = function(){
    //Draw stars
    drawStars();

    //Draw player if alive and play, else do something
    if(player.alive) {
        //To jump and control time
        tapDraw();

        //Draw obstacles
        drawObstacles();

        //As emaple add obstacle every 100 ticks
        if(ticks % 300 == 0){
            for(let i = 0; i < difficulty; i+=10) {
                let size = (Math.random()* (10 - 3) + 3) * vh;
                addObstacle(
                    game.width + Math.random() * game.width/2 * difficulty,
                    Math.random() * (0 - game.height) + game.height,
                    size,
                    size,
                    sprites.meteors[0]
                    );
            }
        }

        //Game ticks +1
        ticks++;

        //Draw bonuses
        //drawBonuses();

         //Draw score
         drawScore(0.02);
         //Draw player
        drawPlayer();

    } else {
        //player dead
        ticks = 0;

        //Save score
        saveScore();

        //Draw user interface
        drawUI();

        saveBonuse();
    }
}